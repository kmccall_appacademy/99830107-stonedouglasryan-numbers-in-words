class Fixnum
  ONES = {
    0 => "zero",
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine"
  }
  TEENS = {
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen"
  }
  TENS = {
    10 => "ten",
    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety"
  }
  ILLIONS = ["thousand", "million", "billion", "trillion"]


  def in_words

    return ONES[self] if ONES.include?(self)
    return TEENS[self] if TEENS.include?(self)
    return TENS[self] if TENS.include?(self)

    num = self.to_s.split("").reverse.each_slice(3).to_a
    num = num.reverse.map(&:reverse)
    num_words = []
    range = num.length / 3

    if num.length % 3 != 0 && num.length > 3
      range += 1
    end

    illions_left = num.length - 2

    num.each_with_index do |n, idx|
      val = 3 - n.length

      if (num[idx].length == 1 || num[idx].length == 3) && ONES[num[idx][0].to_i] != "zero"
        num_words << ONES[num[idx][0].to_i]
      end

      if (num[idx].length == 3 || self.to_s.length == 3) && ONES[num[idx][0].to_i] != "zero"
        num_words << "hundred"
      end

      if num[idx].length == 2 || num[idx].length == 3
        if TEENS.include?((num[idx][1 - val] + num[idx][2 - val]).to_i)
          num_words << (TEENS[(num[idx][1 - val] + num[idx][2 - val]).to_i])
        else
          num_words << (TENS[(num[idx][1 - val] + "0").to_i]) unless
          (num[idx][1 - val] + "0").to_i == 0
          num_words << (ONES[(num[idx][2 - val]).to_i]) unless
          (num[idx][2 - val]).to_i == 0
        end
      end

      if illions_left >= 0 && !ILLIONS.include?(num_words[-1])
        num_words << ILLIONS[illions_left]
      end

      illions_left -= 1 if illions_left >= 0
    end
    num_words.join(" ")
  end
end
